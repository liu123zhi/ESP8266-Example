#ifndef WIFICONFIG_H
#define WIFICONFIG_H

#include <ESP8266WiFi.h>

void autoConfig(void);
String clientID(void);

#endif
