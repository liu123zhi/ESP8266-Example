#include <ESP8266WiFi.h>

void setup() {
  Serial.begin(74880);
  WiFi.mode(WIFI_STA);
  WiFi.begin("taromob", "12345678");
  while (!WiFi.isConnected())
  {
    delay(100);
  }
  Serial.println("WiFi is connected");

  WiFiClient client;
  client.connect("coronavirus-tracker-api.herokuapp.com", 80);
  client.println("GET /v2/latest HTTP/1.1");
  client.println("Host: coronavirus-tracker-api.herokuapp.com");
  client.println("Connection: close");
  client.println();
  String resp = client.readString();
  client.stop();
  Serial.println(resp);
  Serial.println(resp.substring(resp.indexOf("\r\n\r\n")));
}

void loop() {
  // put your main code here, to run repeatedly:
}
