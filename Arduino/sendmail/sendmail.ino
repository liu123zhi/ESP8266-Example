#include <ESP8266WiFi.h>
#include <base64.h>

void sendmail()
{
  WiFiClient mailclient;
  mailclient.connect("smtp.qq.com", 25);
  Serial.println(mailclient.readStringUntil('\n'));
  mailclient.println("ehlo localhost");
  Serial.println(mailclient.readString());
  mailclient.println("auth login");
  Serial.println(mailclient.readStringUntil('\n'));
  mailclient.println(base64::encode("965424572"));
  Serial.println(mailclient.readStringUntil('\n'));
  mailclient.println(base64::encode("授权码"));
  Serial.println(mailclient.readStringUntil('\n'));
  mailclient.println("mail from:<965424572@qq.com>");
  Serial.println(mailclient.readStringUntil('\n'));
  mailclient.println("rcpt to:<18066635882@189.cn>");
  Serial.println(mailclient.readStringUntil('\n'));
  mailclient.println("data");
  Serial.println(mailclient.readStringUntil('\n'));
  mailclient.println("from:<965424572@qq.com>");
  mailclient.println("to:<18066635882@189.cn>");
  mailclient.println("subject:Hello from NodeMCU");
  mailclient.println();
  mailclient.println("Hello, I am NodeMCU!");
  mailclient.println(".");
  Serial.println(mailclient.readStringUntil('\n'));
  mailclient.println("quit");
  Serial.println(mailclient.readStringUntil('\n'));
}

void setup() {
  Serial.begin(74880);
  Serial.println();
  WiFi.mode(WIFI_STA);
  WiFi.begin("taromob", "12345678");
  while (!WiFi.isConnected())
  {
    delay(100);
  }
  Serial.println("WiFi is connected");
  sendmail();
}

void loop() {
  // put your main code here, to run repeatedly:

}
